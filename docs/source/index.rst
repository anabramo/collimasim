.. collimasim documentation master file, created by
   sphinx-quickstart on Fri Jul 23 14:56:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to collimasim's documentation!
======================================

Collimasim provides Python bindings to BDSIM (Geant4) in order to enable collimation studies in pure tracking codes.

Several magical fairies have been contacted about writing the documentation. It is expected to appear here shortly.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   collimasim

..
  Indices and tables
  ==================
..  
  * :ref:`genindex`
  * :ref:`modindex`
  * :ref:`search`
  
